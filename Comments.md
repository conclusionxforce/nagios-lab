# Documenatie/naslag

- Nagios Library: | http://library.nagios.com
- Nagios Support Forum: | http://support.nagios.com/forum
- Nagios Support Center Tickets: | https://support.nagios.com/tickets
- Nagios KB: | https://support.nagios.com/kb/index.php
- Nagios FAQ: | http://support.nagios.com/wiki/index.php/Nagios_XI:FAQs
- Nagios Videos https://www.youtube.com/user/nagiosvideo
- Nagios Developent Guidelines: | https://nagios-plugins.org/doc/guidelines.html
- Change Log algemeen: | https://www.nagios.com/downloads/nagios-xi/change-log/
- Change Log 5: | https://assets.nagios.com/downloads/nagiosxi/CHANGES-5.TXT
- Nagios Core 4 online macro list: | https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/4/en/macrolist.html
- NCPA: | https://www.nagios.org/ncpa/

Klik hier om Nagios XI infographic te bekijken
<img src="images/XI-Infographic-Image.jpg"  width="120" height="120">

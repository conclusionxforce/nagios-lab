# Nagios prutsessie december 2018

<!-- This is commented out.  -->

Welkom bij de labs van de Nagios prutssessie. Bij elke lab (oefening) zie je onderstaande link. Klik er maar eens op..
<details>
 <summary>Spoiler alert! Hint 1</summary>

Op deze plek vind je een hint richting de oplossing van de lab, klik dus **niet** uit automatisme op bovenstaande pijl naar *rechts*. Waarom? Omdat je dan deze hint uitklapt!\
Tot zover de huishoudelijke mededelingen ;-)

</details>

\
In AWS hebben jullie een aantal "persoonlijke" VM's en een aantal gezamelijke die we als groep kunnen gebruiken. 
Klik op plaatje om DNS spiekbriefje te vergroten <img src="images/AWS_DNS.png"  width="40" height="40">

| Medewerker | nagios hostnaam | AWS publieke DNS naam |
| :----------- | :------: | ------------: |
| Pepijn Deutekom | deutekom.xforce.demo   | ec2-34-244-3-35.eu-west-1.compute.amazonaws.com |     |
| Peter Krijgsman | krijgsman.xforce.demo   |  ec2-34-245-31-72.eu-west-1.compute.amazonaws.com       |
| Riemer Palstra       | palstra.xforce.demo   | ec2-34-243-74-119.eu-west-1.compute.amazonaws.com    |
| Erwin Rijvers | rijvers.xforce.demo   | ec2-52-48-19-84.eu-west-1.compute.amazonaws.com     |
| John Romijn | romijn.xforce.demo   | ec2-63-33-69-241.eu-west-1.compute.amazonaws.com      |
| Peter Taheij | taheij.xforce.demo   |ec2-34-255-215-75.eu-west-1.compute.amazonaws.com    |
| Peter Vugs | vugs.xforce.demo   | ec2-52-214-192-134.eu-west-1.compute.amazonaws.com    |

afwezig: Richard Molenkamp, Jaron Tal, Martin Vermeer, Willem Jellema, Jan van de Lagemaat, Remko den Hollander

## Lab 01 - Installeer Nagios XI  [*]

Log in via SSH als `centos` op jullie Nagios server en maak gebruik van de methode om een RPM package te installeren.
Als je de juiste [**repo**](https://repo.nagios.com/?repo=rpm-rhel) toevoegt dan kun je de package `nagiosxi` installeren.

De flow is als volgt:


```mermaid
graph LR;
    A(Putty SSH connectie) -->| Raadpleeg bovengenoemde repo url | B[Repo file installeren]
    B --> C[Nagios XI Package installeren]

```
👉 Op bovenstaande url wordt gesuggereerd om de repo package te installeren met *rpm -Uvh ...* maar jullie zijn natuurlijk eigenwijs/verstandig genoeg om hier *yum install ...* van te maken...
\
👉Laat het updaten van packages (yum update) achterwege. We gaan later een update doen als onderdeel van een lab.

----
Als de package geïnstalleerd is kun je je favoriete webbrowser openen en naar het *publieke* IP adres van de Nagios server.\
👉Je wordt automatisch geredirect naar de PHP installler functie als je http://<public_aws_ip> opent 

```mermaid
graph LR;    
    A(Browser) --> B[Bezoek publieke IP adres]
    B --> C{Maak keuzes install.php}
    C -->|General_System_Setting| D[Settings are saved to DB]
    C -->|Admin_Account_Settings| D[Settings are saved to DB]
    D --> E[Login as nagiosadmin]
```

<details>
 <summary>Spoiler alert! Hint voor lab 01</summary>

```
yum install https://repo.nagios.com/nagios/7/nagios-repo-7-3.el7.noarch.rpm 
yum install nagiosxi
```
</details>


## Lab 02 - Configureer Nagios XI

Configureer Nagios XI als volgt...
* Maak een gebruiker aan onder 'Admin -> Users\Manage Users' van het type 'admin' mét *REST API access*
* Vink 'Allow HTML Tags in Host/Service Status' aan onder Admin -> System Config\System Settings
* Zorg voor dagelijkse backups om 21:00 uur op *local* storage met 14 dagen retentie. Navigeer naar Admin ->  System Backups\Scheduled Backups

## Lab 03 - Self monitoring


De out-of-the-box monitoring van de Nagios server zelf is behoorlijk karig. Als we een Nagios XI systeem hadden inggericht met meerdere filesystemen dan hadden we na een *default install* nog steeds maar één filesysteem die gemonitord wordt.

* Bekijk de monitoring van de Nagios server onder Home -> Details\Hostgroup Overview en klik dan op het blauwe map achter 'localhost'
* Kun je de filesystem monitoring check vinden? 

### 03-a Filesystemen

Stel je voor dat we alle filesystemen in één keer willen checken (voor dit AWS lab doen we even of pseudo filesystemen ook interessant zijn)

* Kopieer onder Configure\Core Config Manager -> Services de filesystem check met de knop `copy`. 
* Zorg voor een *commando* definitie met parameters uit onderstaande tabel (maak een kopie van een bestaande 'check_local_disk' commando-definitie onder Commands\Commands)
* Keer terug naar Monitoring\Service en ernoem de eerder gemaakte kopie service naar de in onderstaande tabel gevonden service naam. 

```
    service name: Disk Usage On All Local Filesystems
    $ARG1$:       15%
    $ARG2$:       10%
    
    command name: check_local_disk_all_fs
    command line: $USER1$/check_disk -w $ARG1$ -c $ARG2$
```

* Als je alle wijzigingen hebt gedaan druk je in "Core Config Manager" op Apply Config. <img src="images/CCM-ApplyConfig.png">

### 03-b Custom plugins toepassen

* Download de 'raw' versie van de volgende custom plugin naar de Windows/MAC/Linux desktop omgeving: https://github.com/jvandermeulen/NagiosPlugins/blob/master/check_reboot_required.bash
* Upload deze plugin naar Nagios XI via de webinterface. Navigatie: Admin ->  System Extensions\Manage Plugins. Deze plugin heeft geen argumenten nodig. Draai de plugin op de commandline of in Nagios XI en bekijk het resultaat. 
* En wat is het resultaat na het updaten van de kernel packages op de Nagios server? 


## Lab 04 - Monitoring Linux m.b.v NCPA Agent

Start de NCPA wizard om een Linux host te monitoren. Adres: nna.nagios.nl poort: 443 (ipv 5693), token: demotoken.
Wizard stap 2: Als het veld 'Host Name:' leeg blijft vul dan 'nna.nagios.nl' in.\
Wizard stap 5: Voeg de host toe aan de hostgroup 'Linux Servers (linux-servers)'

<details>
 <summary>Spoiler alert! Hint lab 04</summary>

Klik hier om image te bekijken
<img src="images/XI_Wizard_NCPA_Linuxhost.png"  width="120" height="120">

</details>

Je vindt online informatie over NCPA op https://www.nagios.org/ncpa/ en je kunt de API en *Live Stats* ontdekken via https://<hostname>:5693 op het systeem waarop je NCPA heb geïnstalleerd.

## Lab 05 - Monitoring SSL en Websites

Start de wizard met de naam "Website (Monitor a website)" en voeg als url in **https://wiki.xforce-conclusion.nl**
\
Vink de *Ping* check uit, deze site staat namelijk geen ICMP verkeer toe.
\
Wat valt je op aan de monitoring? Wat is er aan de hand met de website op het gebied van SSL?\
\
Zijn er meer websites die je wilt monitoren? Ga gerust je gang, maar maak dan alvast een hostgroep aan in Core Config Manager.

<details>
 <summary>Spoiler alert! Hint lab 05</summary>

Klik op image om deze te vergroten
<img src="images/XI_Wizard_Website_01.png"  width="120" height="120">

</details>

## Lab 06 - Nagios check doorsturen naar andere server

Hernoem de host die je met de NCPA wizard hebt gemaakt naar *<jouw_achternaam>.xforce.demo* als hostnaam, zie tabel bovenaan deze pagina (het *address* dient nna.nagios.nl te blijven).\
Stuur de check resultaten door naar de *NRDP* interface op nxi.nagios.nl over HTTP met als Authentication Token: *MyLabOnDecember19* over NRDP. https://assets.nagios.com/downloads/nagiosxi/docs/NRDP_Overview.pdf
\
Hint: Outbound Transfers. Gebruik het *includ&e mask i.p.v. het exclude mask om resultaten uitsluitend bovengenoemde hostnaam door te sturen.


Bonus: stuur een update van jouw lab verrichtingen naar de centrale machine. De commandline tool send_nrdp.sh stelt je in staat om check resultaten te pushen naar een Nagios server

    curl  https://raw.githubusercontent.com/NagiosEnterprises/nrdp/master/clients/send_nrdp.sh -Os
    sudo install -o centos -g centos -m 755 send_nrdp.sh /usr/local/bin
    read -p "Geef de hostnaam op uit de tabel die past bij jouw achternaam (voorbeeld: hollander.xforce.demo): " hostnaam
    send_nrdp.sh -u https://nxi.nagios.nl/nrdp/ -t MyLabOnDecember19 -H ${hostnaam} -s "Lab Status" -o "Finished lab 6" -S 0


## Lab 07 - Host Cloning and Import Wizard

Voor dit lab zijn de volgende voorbereidingen nodig:

* Maak een SSH verbinding met je Nagios XI server en installeer als centos user NCPA en Ansible: `sudo yum install ncpa ansible`
* Voer daarna uit: `ansible localhost -m unarchive -a 'src=http://nxi.nagios.nl/nagios-importfiles.tgz dest=/usr/local/nagios/ remote_src=yes' -b`

* In Core Config Manager importeer je daarna de 3 bestanden in de *cfgprep* directory.\
Navigatie: Configure -> Core Config Manager -> Tools\Import Config Files  <img src="images/XI_Import_Config.png"  width="120" height="120">
* Ga in de side bar naar Monitoring\Host Groups\
en druk op Apply Config. <img src="images/CCM-ApplyConfig.png">
* Bekijk het resultaat onder Home -> Details\Hostgroup Overview

Nu gaan we simuleren dat we een hele batterij aan servers hebben waar we dezelfde checks op willen loslaten.... de snelste manier omdat te bereiken is via de Host Cloning and Import Wizard
Stap 1: Kies linuxhost01.xforce.local als template en selecteer alle checks.\
Stap 2: Plak de volgende hosts data in het scherm 'Import / Cloning Data'

    linuxhost02.xforce.local,localhost,RHEL Satellite Capsule
    linuxhost03.xforce.local,localhost,RHEL IPA
    linuxhost04.xforce.local,localhost,RHEL Ansible Tower
    linuxhost05.xforce.local,localhost,CentOS AWX Server
    
    
en kies de kolomkoppen Field 1: Name, Field 2: Adddress, Field 3: Description.


<details>
 <summary>Spoiler alert! Hint lab 07</summary>

Klik hier om image te bekijken
<img src="images/XI_Cloning_Wizard.png"  width="120" height="120">


</details>

## Lab 08 - Group checks

Doel: optimalizeren beheersgemak, Nagios geschikt maken voor automatische aanmelding via API.\
Hoe te bereiken: een basisset van checks maken voor gelijksoortige hosts. Instelling van default drempelwaardes.

In lab 07 hebben we gezien dat Nagios XI een tool aan boord heeft om checks (selectief) te dupliceren. Het lijkt misschien handig, echter  vanuit beheersperspectief is dit niet ideaal. 
\
We gaan de configuratie ombouwen van...

```mermaid
graph LR;    
    A(CPU service check host 1) --> B[host object 1]
    C(Memory service check host 1) --> B[host object 1]
    E(CPU service check host 2) --> F[host object 2]
    G(Memory service check host 2) --> F[host object 2]
    H(CPU service check host 3) --> I[host object 3]
    J(CPU service check host 3) --> I[host object 3]
```
etcetera
\
naar....

```mermaid
graph LR;    
    A(1 CPU service voor alle hosts) --> C[1 host group ]
    D(1 Memory service voor alle hosts) --> C[1 host group ]
```

* Zorg ervoor dat je bulk clone actie weer ongedaan maakt (door snapshot restore of door handmatige actie op gebied van services en hosts) en zorg ervoor dat de basis checks op linuxhost01.xforce.local 'landen' op de hostgroep waar linuxhost01.xforce.local lid van is.
* Nog tijd over? Hoe zou je ervoor kunnen zorgen dat je drempelwaardes kunt laten afwijken per server i.p.v. 'one fits all'?


## Lab 09 - SNMP Traps ontvangen (***)

Voor monitoring van infrastructuurcomponenten zoals routers, switches en firewalls is het natuurlijk niet mogelijk om een agent te installeren aan zijde van het component.\
Dit is waar éen vant twee varianten van SNMP protocol om de hoek komt kijken. Overigens is SNMP niet bepert tot infracomponenten of Unix/Linux/Windows besturingsystem.. ook applicaties of appliances kunnen "SNMP enabled" zijn.
Daarbij moet verschil worden gemaakt van SNMNP queries (actieve monitoring) en SNMP traps (passieve monitoring). Zie onderstaand schema.

```mermaid
graph LR;
    A[Monitoring server] -->| SNMP Query UDP/TCP 161 | B[SNMP enabled OS/Appliance/Application]
    B[SNMP enabled OS/Appliance/Application] -->| SNMP Trap UDP/TCP 162 | A[Monitoring server]
```

Navigeer naar Admin -> Monitoring Config\SNMP Trap Interface en configureer
* One Click Systems Test
* Add Example Trap Definition (wijzig deze achteraf zodat er een Nagios service wordt aangesproken)
* Send Test Trap
* Send Custom Test Trap

Naslag: https://support.nagios.com/kb/article/nagios-xi-snmp-traps-with-nxti-824.html

-----

[Overige informatie](Comments.md)